package com.cookie.Servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/ShowServlet")
public class ShowServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		
		PrintWriter out = response.getWriter();
		Cookie [] cook=request.getCookies();
		
		if(null != cook && cook[0].getValue().equals("tom")==true && cook[1].getValue().equals("123456")==true && cook[2].getValue().equals("a")==true) {		    
			
			out.println("登录成功！"+"欢迎您："+request.getParameter("name"));
			
		}else {
			out.println("登录失败！");
			out.println("<a href='index.html'>返回登录</a>");
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
