package com.cookie.Servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/IndexServlet")
public class IndexServlet extends HttpServlet {
	

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		
		String name=request.getParameter("name");
		String pwd=request.getParameter("pwd");
		String zd=request.getParameter("zd");
		
		Cookie cookie=new Cookie("name",name);
		Cookie cookie2=new Cookie("pwd",pwd);
		Cookie cookie3=new Cookie("zd",zd);
		
		response.addCookie(cookie);
		response.addCookie(cookie2);
		response.addCookie(cookie3);
		request.getRequestDispatcher("ShowServlet").forward(request,response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
