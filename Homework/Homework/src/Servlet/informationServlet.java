package Servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/servlet/informationServlet")
public class informationServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] hobby = request.getParameterValues("hobby");
		System.out.println(request.getParameter("username"));
		System.out.println(request.getParameter("psw"));
		System.out.println(request.getParameter("sex"));
		System.out.print("爱好:");
		for(int i=0;i<hobby.length;i++)
		{
			System.out.print( " " +hobby[i] );
		}
		System.out.println();
		System.out.println(request.getParameter("county"));
		System.out.println(request.getParameter("email"));
		System.out.println(request.getParameter("textarea"));
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
